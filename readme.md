# Info

Algum material para a UC de Complementos de Programação do cTeSP em Programação de Dispositivos para a Internet

autor: Pedro Cardoso (pcardoso@ualg.pt)

# Configuração

## venv

### criação usando requirements.txt

1. Crie um venv e active-o
1. para instalar os pacotes necessarios instalar os requirements.txt
* pip install -r requirements.txt

## atualização do ficheiro requirements.txt
Se alterar o venv e pretender e pretender atualizar o ficheiro requirements.txt fazer  
* pip freeze > requirements.txt

## activar / desativer

para ativar, em Linux fazer,

`source venv/bin/activate`

e para desativar

`deactivate`

## Jupyter

### Iniciar um ambiente jupyter

o modo mais simples e correr start.sh oui start.bat, dependendo do SO.

### instalar nbextensions

* pip install jupyter_contrib_nbextensions
* jupyter nbextensions_configurator enable --user
* jupyter contrib nbextension install --user

se necessario reiniciar o jupyter. E depois ativar as extensoes na tab correspondente.

### instalar outro tema no jupyter
Correr
* pip install jupyterthemes

e por exemplo

* jt -t monokai -fs 95 -altp -tfs 11 -nfs 115 -cellw 88% -T -nf exosans -f droidmono -altmd -altout -N

### Converter notebook para slideshow

 jupyter nbconvert 01-Classes-introducao.ipynb --to slides --post serve
